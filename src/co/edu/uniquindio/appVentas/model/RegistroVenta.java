/**
 * 
 */
package co.edu.uniquindio.appVentas.model;

import java.time.LocalDate;

/**
 * @author jhomora
 *
 */
public class RegistroVenta {

	private String codigo;
	private LocalDate fechaVenta;
	private DetalleVenta detalleVenta;
	private Cliente cliente;
	private double total;
	
	public final double IVA = 0.19;
	
	/**
	 * @param codigo
	 * @param fechaVenta
	 * @param detalleVenta
	 * @param cliente
	 * @param total
	 */
	public RegistroVenta(String codigo, LocalDate fechaVenta, DetalleVenta detalleVenta,
			Cliente cliente, double total) {
		this.codigo = codigo;
		this.fechaVenta = fechaVenta;
		this.detalleVenta = detalleVenta;
		this.cliente = cliente;
		this.total = total;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the fechaVenta
	 */
	public LocalDate getFechaVenta() {
		return fechaVenta;
	}

	/**
	 * @param fechaVenta the fechaVenta to set
	 */
	public void setFechaVenta(LocalDate fechaVenta) {
		this.fechaVenta = fechaVenta;
	}

	/**
	 * @return the detalleVenta
	 */
	public DetalleVenta getDetalleVenta() {
		return detalleVenta;
	}

	/**
	 * @param detalleVenta the detalleVenta to set
	 */
	public void setDetalleVenta(DetalleVenta detalleVenta) {
		this.detalleVenta = detalleVenta;
	}

	/**
	 * @return the cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}

	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the total
	 */
	public double getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(double total) {
		this.total = total;
	}
	
}
