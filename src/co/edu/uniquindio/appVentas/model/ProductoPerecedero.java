/**
 * 
 */
package co.edu.uniquindio.appVentas.model;

import java.time.LocalDate;

/**
 * @author jhomora
 *
 */
public class ProductoPerecedero extends Producto {

	private LocalDate fechaVencimiento;
	
	/**
	 * @param codigo
	 * @param nombre
	 * @param descripcion
	 * @param valorUnitario
	 * @param cantExistenciaProducto
	 */
	public ProductoPerecedero(String codigo, String nombre, String descripcion, double valorUnitario,
			int cantExistenciaProducto, LocalDate fechaVencimiento) {
		super(codigo, nombre, descripcion, valorUnitario, cantExistenciaProducto);

		this.fechaVencimiento = fechaVencimiento;
	}

	/**
	 * @return the fechaVencimiento
	 */
	public LocalDate getFechaVencimiento() {
		return fechaVencimiento;
	}

	/**
	 * @param fechaVencimiento the fechaVencimiento to set
	 */
	public void setFechaVencimiento(LocalDate fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	
	

}
