/**
 * 
 */
package co.edu.uniquindio.appVentas.exception;

/**
 * @author jhomora
 *
 */
public class ProductoDuplicadoException extends Exception {

	/**
	 * 
	 */
	public ProductoDuplicadoException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public ProductoDuplicadoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
