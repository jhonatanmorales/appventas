/**
 * 
 */
package co.edu.uniquindio.appVentas.model;

/**
 * @author jhomora
 *
 */
public class PersonaJuridica extends Cliente {

	private String nit;
	
	/**
	 * 
	 */
	public PersonaJuridica() {
		super();
	}
	
	/**
	 * @param nombres
	 * @param apellidos
	 * @param direccion
	 * @param telefono
	 */
	public PersonaJuridica(String nombres, String apellidos, String direccion, String telefono,
			String nit) {
		super(nombres, apellidos, direccion, telefono);
		
		this.nit = nit;
	}

	/**
	 * @return the nit
	 */
	public String getNit() {
		return nit;
	}

	/**
	 * @param nit the nit to set
	 */
	public void setNit(String nit) {
		this.nit = nit;
	}

}
