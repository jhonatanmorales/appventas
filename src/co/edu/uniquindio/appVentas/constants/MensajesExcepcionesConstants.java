/**
 * 
 */
package co.edu.uniquindio.appVentas.constants;

/**
 * @author jhomora
 *
 */
public class MensajesExcepcionesConstants {

	public static final String CLIENTE_DUPLICADO = "El cliente ya existe en el sistema";
	
	/**
	 * PRODUCTOS
	 */
	
	public static final String PRODUCTO_DUPLICADO = "El producto ya existe en el sistema";
	public static final String NO_SELECCION_PRODUCTO = "Debe seleccionar un producto";

}
