/**
 * 
 */
package co.edu.uniquindio.appVentas.model;

import java.time.LocalDate;

/**
 * @author jhomora
 *
 */
public class PersonaNatural extends Cliente {

	private String email;
	private LocalDate fechaNacimiento;
	
	/**
	 * 
	 */
	public PersonaNatural() {
		super();
	}
	
	/**
	 * @param nombres
	 * @param apellidos
	 * @param direccion
	 * @param telefono
	 */
	public PersonaNatural(String nombres, String apellidos, String direccion, String telefono,
			String email, LocalDate fechaNacimiento) {
		super(nombres, apellidos, direccion, telefono);
		
		this.email = email;
		this.fechaNacimiento = fechaNacimiento;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the fechaNacimiento
	 */
	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	/**
	 * @param fechaNacimiento the fechaNacimiento to set
	 */
	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	
}
