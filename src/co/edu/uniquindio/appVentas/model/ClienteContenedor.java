/**
 * 
 */
package co.edu.uniquindio.appVentas.model;

import java.time.LocalDate;

/**
 * @author jhomora
 *
 */
public class ClienteContenedor extends Cliente {

	private String email;
	private LocalDate fechaNacimiento;
	private String nit;
	
	/**
	 * 
	 */
	public ClienteContenedor() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param nombres
	 * @param apellidos
	 * @param direccion
	 * @param telefono
	 */
	public ClienteContenedor(String nombres, String apellidos, String direccion, String telefono) {
		super(nombres, apellidos, direccion, telefono);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the fechaNacimiento
	 */
	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	/**
	 * @param fechaNacimiento the fechaNacimiento to set
	 */
	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	/**
	 * @return the nit
	 */
	public String getNit() {
		return nit;
	}

	/**
	 * @param nit the nit to set
	 */
	public void setNit(String nit) {
		this.nit = nit;
	}

	
}
