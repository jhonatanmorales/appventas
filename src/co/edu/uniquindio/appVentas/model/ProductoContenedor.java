/**
 * 
 */
package co.edu.uniquindio.appVentas.model;

import java.time.LocalDate;

import co.edu.uniquindio.appVentas.enums.PaisOrigenEnum;

/**
 * @author jhomora
 *
 */
public class ProductoContenedor extends Producto {

	private LocalDate fechaVencimiento;
	private String codigoAprovacion;
	private int tempRefrigeracion;
	private LocalDate fechaEnvasado;
	private int pesoEnvase;
	private PaisOrigenEnum paisOrigen;
	
	/**
	 * 
	 */
	public ProductoContenedor() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param codigo
	 * @param nombre
	 * @param descripcion
	 * @param valorUnitario
	 * @param cantExistenciaProducto
	 */
	public ProductoContenedor(String codigo, String nombre, String descripcion, int valorUnitario,
			int cantExistenciaProducto) {
		super(codigo, nombre, descripcion, valorUnitario, cantExistenciaProducto);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the fechaVencimiento
	 */
	public LocalDate getFechaVencimiento() {
		return fechaVencimiento;
	}

	/**
	 * @param fechaVencimiento the fechaVencimiento to set
	 */
	public void setFechaVencimiento(LocalDate fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	/**
	 * @return the codigoAprovacion
	 */
	public String getCodigoAprovacion() {
		return codigoAprovacion;
	}

	/**
	 * @param codigoAprovacion the codigoAprovacion to set
	 */
	public void setCodigoAprovacion(String codigoAprovacion) {
		this.codigoAprovacion = codigoAprovacion;
	}

	/**
	 * @return the tempRefrigeracion
	 */
	public int getTempRefrigeracion() {
		return tempRefrigeracion;
	}

	/**
	 * @param tempRefrigeracion the tempRefrigeracion to set
	 */
	public void setTempRefrigeracion(int tempRefrigeracion) {
		this.tempRefrigeracion = tempRefrigeracion;
	}

	/**
	 * @return the fechaEnvasado
	 */
	public LocalDate getFechaEnvasado() {
		return fechaEnvasado;
	}

	/**
	 * @param fechaEnvasado the fechaEnvasado to set
	 */
	public void setFechaEnvasado(LocalDate fechaEnvasado) {
		this.fechaEnvasado = fechaEnvasado;
	}

	/**
	 * @return the pesoEnvase
	 */
	public int getPesoEnvase() {
		return pesoEnvase;
	}

	/**
	 * @param pesoEnvase the pesoEnvase to set
	 */
	public void setPesoEnvase(int pesoEnvase) {
		this.pesoEnvase = pesoEnvase;
	}

	/**
	 * @return the paisOrigen
	 */
	public PaisOrigenEnum getPaisOrigen() {
		return paisOrigen;
	}

	/**
	 * @param paisOrigen the paisOrigen to set
	 */
	public void setPaisOrigen(PaisOrigenEnum paisOrigen) {
		this.paisOrigen = paisOrigen;
	}

	
}
