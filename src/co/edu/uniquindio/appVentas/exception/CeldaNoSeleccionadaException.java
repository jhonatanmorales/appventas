/**
 * 
 */
package co.edu.uniquindio.appVentas.exception;

/**
 * @author jhomora
 *
 */
public class CeldaNoSeleccionadaException extends Exception {

	/**
	 * 
	 */
	public CeldaNoSeleccionadaException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public CeldaNoSeleccionadaException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
}
