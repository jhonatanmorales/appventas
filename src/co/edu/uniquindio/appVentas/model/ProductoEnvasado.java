/**
 * 
 */
package co.edu.uniquindio.appVentas.model;

import java.time.LocalDate;

import co.edu.uniquindio.appVentas.enums.PaisOrigenEnum;

/**
 * @author jhomora
 *
 */
public class ProductoEnvasado extends Producto {

	private LocalDate fechaEnvasado;
	private double pesoEnvase;
	private PaisOrigenEnum paisOrigen;
	
	/**
	 * @param codigo
	 * @param nombre
	 * @param descripcion
	 * @param valorUnitario
	 * @param cantExistenciaProducto
	 */
	public ProductoEnvasado(String codigo, String nombre, String descripcion, double valorUnitario,
			int cantExistenciaProducto, LocalDate fechaEnvasado, double pesoEnvase,
			PaisOrigenEnum paisOrigen) {
		super(codigo, nombre, descripcion, valorUnitario, cantExistenciaProducto);

		this.fechaEnvasado = fechaEnvasado;
		this.pesoEnvase = pesoEnvase;
		this.paisOrigen = paisOrigen;
	}

	/**
	 * @return the fechaEnvasado
	 */
	public LocalDate getFechaEnvasado() {
		return fechaEnvasado;
	}

	/**
	 * @param fechaEnvasado the fechaEnvasado to set
	 */
	public void setFechaEnvasado(LocalDate fechaEnvasado) {
		this.fechaEnvasado = fechaEnvasado;
	}

	/**
	 * @return the pesoEnvase
	 */
	public double getPesoEnvase() {
		return pesoEnvase;
	}

	/**
	 * @param pesoEnvase the pesoEnvase to set
	 */
	public void setPesoEnvase(double pesoEnvase) {
		this.pesoEnvase = pesoEnvase;
	}

	/**
	 * @return the paisOrigen
	 */
	public PaisOrigenEnum getPaisOrigen() {
		return paisOrigen;
	}

	
}
