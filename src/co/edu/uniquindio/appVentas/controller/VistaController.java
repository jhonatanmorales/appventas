package co.edu.uniquindio.appVentas.controller;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;
import java.util.ResourceBundle;

import co.edu.uniquindio.appVentas.constants.MensajesExcepcionesConstants;
import co.edu.uniquindio.appVentas.constants.MensajesInformacionConstants;
import co.edu.uniquindio.appVentas.enums.PaisOrigenEnum;
import co.edu.uniquindio.appVentas.exception.CeldaNoSeleccionadaException;
import co.edu.uniquindio.appVentas.exception.ClienteDuplicadoException;
import co.edu.uniquindio.appVentas.exception.ProductoDuplicadoException;
import co.edu.uniquindio.appVentas.model.Cliente;
import co.edu.uniquindio.appVentas.model.ClienteContenedor;
import co.edu.uniquindio.appVentas.model.PersonaJuridica;
import co.edu.uniquindio.appVentas.model.PersonaNatural;
import co.edu.uniquindio.appVentas.model.ProductoContenedor;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;

public class VistaController implements Initializable {

	private PersonaJuridica juridica;
	private PersonaNatural natural;
	private ClienteContenedor contenedorJuridico, contenedorNatural, contenedor;
	private ProductoContenedor productoContenedor;
	private ArrayList<PersonaJuridica> clientesJuridicos;
	private ArrayList<ProductoContenedor> listaProductosArray;
	private ObservableList<ClienteContenedor> listaClientes;
	private ObservableList<ProductoContenedor> listaProductos;
	private ObservableList<PaisOrigenEnum> listaPaises;
	private ArrayList<PersonaNatural> clientesNaturales;
	
	
    @FXML // fx:id="DateFechaNacimiento"
    private DatePicker dateFechaNacimiento; // Value injected by FXMLLoader

    @FXML // fx:id="btnActualizar"
    private Button btnActualizar; // Value injected by FXMLLoader

    @FXML // fx:id="btnAgregar"
    private Button btnAgregar; // Value injected by FXMLLoader

    @FXML // fx:id="btnLimpiar"
    private Button btnLimpiar; // Value injected by FXMLLoader

    @FXML // fx:id="colAccion"
    private TableColumn<Button, String> colAccion; // Value injected by FXMLLoader

    @FXML // fx:id="colApellido"
    private TableColumn<Cliente, String> colApellido; // Value injected by FXMLLoader

    @FXML // fx:id="colCorreo"
    private TableColumn<PersonaNatural, String> colCorreo; // Value injected by FXMLLoader

    @FXML // fx:id="colDireccion"
    private TableColumn<Cliente, String> colDireccion; // Value injected by FXMLLoader

    @FXML // fx:id="colFNacimiento"
    private TableColumn<PersonaNatural, String> colFNacimiento; // Value injected by FXMLLoader

    @FXML // fx:id="colNit"
    private TableColumn<PersonaJuridica, String> colNit; // Value injected by FXMLLoader

    @FXML // fx:id="colNombre"
    private TableColumn<Cliente, String> colNombre; // Value injected by FXMLLoader

    @FXML // fx:id="colTelefono"
    private TableColumn<Cliente, String> colTelefono; // Value injected by FXMLLoader
    
    @FXML // fx:id="colAccion"
    private TableColumn<Cliente, String> colDocumento; // Value injected by FXMLLoader

    @FXML // fx:id="txtApellido"
    private TextField txtApellido; // Value injected by FXMLLoader

    @FXML // fx:id="txtCorreo"
    private TextField txtCorreo; // Value injected by FXMLLoader

    @FXML // fx:id="txtDocumento"
    private TextField txtDireccion; // Value injected by FXMLLoader

    @FXML // fx:id="txtNit"
    private TextField txtNit; // Value injected by FXMLLoader

    @FXML // fx:id="txtNombre"
    private TextField txtNombre; // Value injected by FXMLLoader

    @FXML // fx:id="txtTelefono"
    private TextField txtTelefono; // Value injected by FXMLLoader
    
    @FXML
    private TextField txtId;
    
    @FXML
    private CheckBox checkPerNatural, checkPerJuridica;
    
    @FXML
    private Label lblNit, lblCorreo, lblFNacimiento, lblId;
    
    @FXML
    private TableView<ClienteContenedor> tableCliente;
    
    
    /**
     * ###############################################################################
     * ###############################PRODUCTOS######################################
     * ##############################################################################
     */

    @FXML 
    private Button btnAgregarProducto; // Value injected by FXMLLoader

    @FXML 
    private Button btnActualizarProducto; // Value injected by FXMLLoader

    @FXML 
    private Button btnLimpiarProducto; // Value injected by FXMLLoader
    
    @FXML 
    private Button btnEliminarProducto; // Value injected by FXMLLoader
    
    @FXML // fx:id="checkEnvasado"
    private CheckBox checkEnvasado; // Value injected by FXMLLoader

    @FXML // fx:id="checkPerecedero"
    private CheckBox checkPerecedero; // Value injected by FXMLLoader

    @FXML // fx:id="checkRefrigerado"
    private CheckBox checkRefrigerado; // Value injected by FXMLLoader

    @FXML // fx:id="colNombre"
    private TableColumn<ProductoContenedor, String> colNombreP; // Value injected by FXMLLoader
    
    @FXML // fx:id="colCantidad"
    private TableColumn<ProductoContenedor, String> colCantidad; // Value injected by FXMLLoader

    @FXML // fx:id="colCodAprovacion"
    private TableColumn<ProductoContenedor, String> colCodAprovacion; // Value injected by FXMLLoader

    @FXML // fx:id="colCodigo"
    private TableColumn<ProductoContenedor, String> colCodigo; // Value injected by FXMLLoader

    @FXML // fx:id="colFeEnvasado"
    private TableColumn<ProductoContenedor, String> colFeEnvasado; // Value injected by FXMLLoader

    @FXML // fx:id="colFeVencimiento"
    private TableColumn<ProductoContenedor, String> colFeVencimiento; // Value injected by FXMLLoader
    
    @FXML // fx:id="colPaisOrigen"
    private TableColumn<ProductoContenedor, String> colPaisOrigen; // Value injected by FXMLLoader

    @FXML // fx:id="colPesoEnvase"
    private TableColumn<ProductoContenedor, String> colPesoEnvase; // Value injected by FXMLLoader

    @FXML // fx:id="colTiemRefrigeracion"
    private TableColumn<ProductoContenedor, String> colTiemRefrigeracion; // Value injected by FXMLLoader

    @FXML // fx:id="colValor"
    private TableColumn<ProductoContenedor, String> colValor; // Value injected by FXMLLoader

    @FXML // fx:id="coldescripcion"
    private TableColumn<ProductoContenedor, String> colDescripcion; // Value injected by FXMLLoader

    @FXML // fx:id="dateFeEnvasado"
    private DatePicker dateFeEnvasado; // Value injected by FXMLLoader

    @FXML // fx:id="dateVencimiento"
    private DatePicker dateVencimiento; // Value injected by FXMLLoader

    @FXML // fx:id="lblCodAprovacion"
    private Label lblCodAprovacion; // Value injected by FXMLLoader

    @FXML // fx:id="lblFVencimiento"
    private Label lblFVencimiento; // Value injected by FXMLLoader

    @FXML // fx:id="lblFeEnvasado"
    private Label lblFeEnvasado; // Value injected by FXMLLoader

    @FXML // fx:id="lblPaisOrigen"
    private Label lblPaisOrigen; // Value injected by FXMLLoader

    @FXML // fx:id="lblPesoEnvase"
    private Label lblPesoEnvase; // Value injected by FXMLLoader
    
    @FXML
    private Label lblTiemRefrigeracion;

    @FXML // fx:id="lblTiemRefrigeracion"
    private TextField txtTiemRefrigeracion; // Value injected by FXMLLoader

    @FXML // fx:id="tablaProductos"
    private TableView<ProductoContenedor> tablaProductos; // Value injected by FXMLLoader

    @FXML // fx:id="txtNombre"
    private TextField txtNombreP; // Value injected by FXMLLoader
    
    @FXML // fx:id="txtCantidad"
    private TextField txtCantidad; // Value injected by FXMLLoader

    @FXML // fx:id="txtCodAprovacion"
    private TextField txtCodAprobacion; // Value injected by FXMLLoader

    @FXML // fx:id="txtCodigo"
    private TextField txtCodigo; // Value injected by FXMLLoader

    @FXML // fx:id="txtDescripcion"
    private TextArea txtDescripcion; // Value injected by FXMLLoader

    @FXML // fx:id="txtPaisOrigen"
    private ComboBox<PaisOrigenEnum> txtPaisOrigen; // Value injected by FXMLLoader

    @FXML // fx:id="txtPesoEnvase"
    private TextField txtPesoEnvase; // Value injected by FXMLLoader

    @FXML // fx:id="txtValor"
    private TextField txtValor; // Value injected by FXMLLoader

    /**
     * ###############################################################################
     * ###############################VENTAS######################################
     * ##############################################################################
     */
    

    @FXML // fx:id="btnActualizarVenta"
    private Button btnActualizarVenta; // Value injected by FXMLLoader


    @FXML // fx:id="btnAgregarVenta"
    private Button btnAgregarVenta; // Value injected by FXMLLoader

    @FXML // fx:id="btnEliminarVenta"
    private Button btnEliminarVenta; // Value injected by FXMLLoader


    @FXML // fx:id="btnLimpiarVenta"
    private Button btnLimpiarVenta; // Value injected by FXMLLoader


    @FXML // fx:id="colCantProductoVenta"
    private TableColumn<?, ?> colCantProductoVenta; // Value injected by FXMLLoader


    @FXML // fx:id="colClienteVenta"
    private TableColumn<?, ?> colClienteVenta; // Value injected by FXMLLoader


    @FXML // fx:id="colCodVenta"
    private TableColumn<?, ?> colCodVenta; // Value injected by FXMLLoader


    @FXML // fx:id="colFeVenta"
    private TableColumn<?, ?> colFeVenta; // Value injected by FXMLLoader


    @FXML // fx:id="colProductoVenta"
    private TableColumn<?, ?> colProductoVenta; // Value injected by FXMLLoader


    @FXML // fx:id="colTotalVenta"
    private TableColumn<?, ?> colTotalVenta; // Value injected by FXMLLoader


    @FXML // fx:id="comboBoxCliente"
    private ComboBox<?> comboBoxCliente; // Value injected by FXMLLoader

    @FXML // fx:id="comboBoxProductos"
    private ComboBox<?> comboBoxProductos; // Value injected by FXMLLoader


    @FXML // fx:id="dateFechaVenta"
    private DatePicker dateFechaVenta; // Value injected by FXMLLoader

    @FXML // fx:id="tablaVenta"
    private TableView<?> tablaVenta; // Value injected by FXMLLoader


    @FXML // fx:id="txtCantProducto"
    private TextField txtCantProducto; // Value injected by FXMLLoader


    @FXML // fx:id="txtCodVenta"
    private TextField txtCodVenta; // Value injected by FXMLLoader


    @FXML // fx:id="txtDetalleVenta"
    private TextArea txtDetalleVenta; // Value injected by FXMLLoader


    @FXML // fx:id="txtIva"
    private TextField txtIva; // Value injected by FXMLLoader


    @FXML // fx:id="txtSubTotal"
    private TextField txtSubTotal; // Value injected by FXMLLoader


    @FXML // fx:id="txtTotal"
    private TextField txtTotal; // Value injected by FXMLLoader

    
    @FXML
    public void GuardarCliente(ActionEvent e) {
    	if(checkPerNatural.isSelected()) {
    		try {
    			natural = new PersonaNatural();
        		
    			natural.setDocumento(txtId.getText());
        		natural.setNombres(txtNombre.getText());
        		natural.setApellidos(txtApellido.getText());
        		natural.setTelefono(txtTelefono.getText());
        		natural.setDireccion(txtDireccion.getText());
        		natural.setEmail(txtCorreo.getText());
        		natural.setFechaNacimiento(dateFechaNacimiento.getValue());
        		
        		if(natural != null) {
        			ModeloCrudController.getInstacia().agregarClienteNatural(natural);
        			cargarDatosPerNatural();
        		}
        		
        		limpiarCampos(e);
        		
			} catch (ClienteDuplicadoException e2) {
				 Alert alertaDatos = new Alert(AlertType.WARNING);
                 alertaDatos.setHeaderText(null);
                 alertaDatos.setTitle("Datos erroneos");
                 alertaDatos.setContentText(e2.getMessage());
                 alertaDatos.showAndWait();
			}
    		
    	}
    	
    	if(checkPerJuridica.isSelected()){
    		try {
    			juridica = new PersonaJuridica();
        		
    			juridica.setNombres(txtNombre.getText());
    			juridica.setApellidos(txtApellido.getText());
    			juridica.setTelefono(txtTelefono.getText());
    			juridica.setDireccion(txtDireccion.getText());
    			juridica.setNit(txtNit.getText());
        		
        		if(juridica != null) {
        			ModeloCrudController.getInstacia().agregarClienteJuridico(juridica);
        			cargarDatosPerJuridica();
        		}
        		
        		limpiarCampos(e);
			} catch (ClienteDuplicadoException e2) {
				 Alert alertaDatos = new Alert(AlertType.WARNING);
                 alertaDatos.setHeaderText(null);
                 alertaDatos.setTitle("Datos erroneos");
                 alertaDatos.setContentText(e2.getMessage());
                 alertaDatos.showAndWait();
			}
    		
    	}
    }
    
    @FXML
    public void inhabilitarCheckBox1(MouseEvent e) {
    	if(checkPerNatural.isSelected()) {
    		checkPerJuridica.setDisable(true);
    		txtCorreo.setDisable(false);
    		dateFechaNacimiento.setDisable(false);
    		lblCorreo.setDisable(false);
    		lblFNacimiento.setDisable(false);
			lblId.setDisable(false);
			txtId.setDisable(false);
    	}else if(!checkPerNatural.isSelected()){
    		checkPerJuridica.setDisable(false);
    		lblCorreo.setDisable(true);
    		lblFNacimiento.setDisable(true);
    		txtCorreo.setDisable(true);
    		dateFechaNacimiento.setDisable(true);
			lblId.setDisable(true);
			txtId.setDisable(true);
    	}
    }
    
    @FXML
    public void inhabilitarCheckBox2(MouseEvent e) {
    	if(checkPerJuridica.isSelected()) {
    		checkPerNatural.setDisable(true);
    		lblNit.setDisable(false);
    		txtNit.setDisable(false);
    	}else if(!checkPerJuridica.isSelected()){
    		checkPerNatural.setDisable(false);
    		lblNit.setDisable(true);
    		txtNit.setDisable(true);
    	}
    }
    
    public void eliminarClienteNatural(ClienteContenedor clienteNatural) {
    	
    	clienteNatural.getBtnEliminar().setDisable(false);
    	clienteNatural.getBtnEliminar().setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				Alert alertaCerrar = new Alert(AlertType.CONFIRMATION);
				alertaCerrar.setHeaderText(null);
				alertaCerrar.setTitle("Eliminar");
				alertaCerrar.setContentText(MensajesInformacionConstants.CLIENTE_CONFIRMACION);

				// limpia los botones del alert
				alertaCerrar.getButtonTypes().clear();
				// agraga botones al alert
				alertaCerrar.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

				// clase generica que captura el tipo de elemento que devuelve el panel de
				// dialogo
				Optional<ButtonType> opcion = alertaCerrar.showAndWait();
				// verifica que la opcion sea aceptar
				if (opcion.get() == ButtonType.OK) {
					if(ModeloCrudController.getInstacia().eliminarClienteNatural(clienteNatural.getDocumento())) {
						listaClientes.remove(clienteNatural);
						tableCliente.refresh();
						limpiarCampos(event);
						
						Alert alertaDatos = new Alert(AlertType.CONFIRMATION);
						alertaDatos.setHeaderText(null);
						alertaDatos.setTitle("Informacion");
						alertaDatos.setContentText(MensajesInformacionConstants.CLIENTE_ELIMINADO);
						alertaDatos.showAndWait();
					}
				}
				
			}
		});
    	
    }
    
    public void eliminarClienteJuridico(ClienteContenedor clienteJuridico) {

		clienteJuridico.getBtnEliminar().setDisable(false);
		clienteJuridico.getBtnEliminar().setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				Alert alertaCerrar = new Alert(AlertType.CONFIRMATION);
				alertaCerrar.setHeaderText(null);
				alertaCerrar.setTitle("Eliminar");
				alertaCerrar.setContentText(MensajesInformacionConstants.CLIENTE_CONFIRMACION);

				// limpia los botones del alert
				alertaCerrar.getButtonTypes().clear();
				// agraga botones al alert
				alertaCerrar.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

				// clase generica que captura el tipo de elemento que devuelve el panel de
				// dialogo
				Optional<ButtonType> opcion = alertaCerrar.showAndWait();
				// verifica que la opcion sea aceptar
				if (opcion.get() == ButtonType.OK) {
					if(ModeloCrudController.getInstacia().eliminarClienteJuridico(clienteJuridico.getNit())) {
						listaClientes.remove(clienteJuridico);
						tableCliente.refresh();
						limpiarCampos(event);
						
						Alert alertaDatos = new Alert(AlertType.CONFIRMATION);
						alertaDatos.setHeaderText(null);
						alertaDatos.setTitle("Informacion");
						alertaDatos.setContentText(MensajesInformacionConstants.CLIENTE_ELIMINADO);
						alertaDatos.showAndWait();
					}
				}
				
			}
		});
		
    }
    
    @FXML
    public void capturarInfoEliminar(MouseEvent e) {
    	ClienteContenedor cliContenedor = tableCliente.getSelectionModel().getSelectedItem();
    	
    	if(cliContenedor != null) {
    		if(!cliContenedor.getNit().equals("N/A")) {
        		eliminarClienteJuridico(cliContenedor);
        	}else {
        		eliminarClienteNatural(cliContenedor);
        	}
    	}
    }
    
    @FXML
    public void capturarInfoActualizar(MouseEvent e) {
    	ClienteContenedor cliContenedor = tableCliente.getSelectionModel().getSelectedItem();
    	
    	if(cliContenedor != null) {
    		txtNombre.setText(cliContenedor.getNombres());
    		txtApellido.setText(cliContenedor.getApellidos());
    		txtTelefono.setText(cliContenedor.getTelefono());
    		txtDireccion.setText(cliContenedor.getDireccion());
    		
    		if(!cliContenedor.getNit().equals("N/A")) {
    			checkPerJuridica.setDisable(false);
    			checkPerJuridica.setSelected(true);
    			txtNit.setText(cliContenedor.getNit());
    			
    			checkPerNatural.setDisable(true);
    			checkPerNatural.setSelected(false);
    			txtCorreo.setDisable(true);
    			dateFechaNacimiento.setDisable(true);
    			txtCorreo.clear();
    			dateFechaNacimiento.getEditor().clear();
    			dateFechaNacimiento.setPromptText("");
    			lblId.setDisable(true);
    			txtId.setDisable(true);
    			txtId.clear();
    			
        	}else {
        		checkPerNatural.setDisable(false);
        		checkPerNatural.setSelected(true);
    			txtCorreo.setDisable(false);
    			lblCorreo.setDisable(false);
    			lblFNacimiento.setDisable(false);
    			dateFechaNacimiento.setDisable(true);
    			txtCorreo.setText(cliContenedor.getEmail());
    			dateFechaNacimiento.setPromptText(cliContenedor.getFechaNacimiento().toString());
    			txtId.setText(cliContenedor.getDocumento());
    			lblId.setDisable(true);
    			txtId.setDisable(true);
    			
    			checkPerJuridica.setDisable(true);
    			checkPerJuridica.setSelected(false);
    			txtNit.setDisable(true);
    			txtNit.clear();
    			
        	}
    		
    		actualizarCliente(cliContenedor);
    		
    	}
    }
    
    public void actualizarCliente(ClienteContenedor clienteContenedor) {
    	
    		btnActualizar.setOnAction(new EventHandler<ActionEvent>() {
				
				@Override
				public void handle(ActionEvent event) {
					clienteContenedor.setNombres(txtNombre.getText());
		    		clienteContenedor.setApellidos(txtApellido.getText());
					clienteContenedor.setTelefono(txtTelefono.getText());
					clienteContenedor.setDireccion(txtDireccion.getText());
					if(clienteContenedor.getNit().equals("N/A")) {
						clienteContenedor.setEmail(txtCorreo.getText());
						clienteContenedor.setFechaNacimiento(dateFechaNacimiento.getValue());
						clienteContenedor.setNit("N/A");
					}else {
						clienteContenedor.setNit(txtNit.getText());
						clienteContenedor.setEmail("N/A");
						clienteContenedor.setFechaNacimiento(null);
					}
					
					if(ModeloCrudController.getInstacia().actualizarCliente(clienteContenedor)) {
						tableCliente.refresh();
						limpiarCampos(event);
					}
				}
			});
    }
    
    public void cargarDatosPerNatural() {
    	contenedorNatural = new ClienteContenedor();
    	
    	
    	for (PersonaNatural personaNatural : clientesNaturales) {
    		
    		mostrarCamposEntidad();
    		
    		contenedorNatural.setDocumento(personaNatural.getDocumento());
    		contenedorNatural.setNombres(personaNatural.getNombres());
    		contenedorNatural.setApellidos(personaNatural.getApellidos());
    		contenedorNatural.setTelefono(personaNatural.getTelefono());
    		contenedorNatural.setDireccion(personaNatural.getDireccion());
    		contenedorNatural.setBtnEliminar(crearBotonEliminar("X"));
    		contenedorNatural.setEmail(personaNatural.getEmail());
    		contenedorNatural.setFechaNacimiento(personaNatural.getFechaNacimiento());
    		contenedorNatural.setNit("N/A");
    		
		}
    	listaClientes.add(contenedorNatural);
    	
    	tableCliente.setItems(listaClientes);
    }
    
    public void cargarDatosPerJuridica() {
    	contenedorJuridico = new ClienteContenedor();
    	for (PersonaJuridica personaJuridica : clientesJuridicos) {
    		
    		mostrarCamposEntidad();
    		
    		contenedorJuridico.setNombres(personaJuridica.getNombres());
    		contenedorJuridico.setApellidos(personaJuridica.getApellidos());
    		contenedorJuridico.setTelefono(personaJuridica.getTelefono());
    		contenedorJuridico.setDireccion(personaJuridica.getDireccion());
    		contenedorJuridico.setBtnEliminar(crearBotonEliminar("X"));
    		contenedorJuridico.setEmail("N/A");
    		contenedorJuridico.setDocumento("N/A");
    		contenedorJuridico.setFechaNacimiento(null);
    		contenedorJuridico.setNit(personaJuridica.getNit());
    	
		}
    	listaClientes.add(contenedorJuridico);
    	
    	tableCliente.setItems(listaClientes);
    	
    }
    
    @FXML
    public void limpiarCampos(ActionEvent e) {
    	txtNombre.clear();
		txtApellido.clear();
		txtTelefono.clear();
		txtDireccion.clear();
		txtCorreo.clear();
		dateFechaNacimiento.getEditor().clear();
		txtNit.clear();
		txtId.clear();
		checkPerJuridica.setSelected(false);
		checkPerNatural.setSelected(false);
		
    	if(!checkPerNatural.isSelected()){
    		checkPerJuridica.setDisable(false);
    		lblCorreo.setDisable(true);
    		lblFNacimiento.setDisable(true);
    		txtCorreo.setDisable(true);
    		dateFechaNacimiento.setDisable(true);
			lblId.setDisable(true);
			txtId.setDisable(true);
    	}
    	
    	if(!checkPerJuridica.isSelected()){
    		checkPerNatural.setDisable(false);
    		lblNit.setDisable(true);
    		txtNit.setDisable(true);
    	}
    }
    
    public Button crearBotonEliminar(String nombre) {
    	Button btnEliminar = new Button(nombre);
    	btnEliminar.setShape(new Circle(18));
    	btnEliminar.setStyle("-fx-base: rgb(243, 43, 43)");
    	btnEliminar.setDisable(true);
    	
    	return btnEliminar;
    }
    
    public void mostrarCamposEntidad() {
    	colDocumento.setCellValueFactory(
                new PropertyValueFactory<>("documento"));
    	colNombre.setCellValueFactory(
                new PropertyValueFactory<>("nombres"));
        colApellido.setCellValueFactory(
                new PropertyValueFactory<>("apellidos"));
        colTelefono.setCellValueFactory(
                new PropertyValueFactory<>("telefono"));
        colDireccion.setCellValueFactory(
                new PropertyValueFactory<>("direccion"));
        colCorreo.setCellValueFactory(
                new PropertyValueFactory<>("email"));
        colFNacimiento.setCellValueFactory(
                new PropertyValueFactory<>("fechaNacimiento"));
        colNit.setCellValueFactory(
                new PropertyValueFactory<>("nit"));
        colAccion.setCellValueFactory(new PropertyValueFactory<>("btnEliminar"));
    }

    /**
     * ###############################################################################
     * ###############################ACCIONES PRODUCTOS######################################
     * ##############################################################################
     */

    @FXML
    public void capturarInfoActualizarProducto(MouseEvent event) {
    	ProductoContenedor producto = tablaProductos.getSelectionModel().getSelectedItem();
    	
    	if(producto != null) {
    		txtCodigo.setText(producto.getCodigo());
    		txtCodigo.setDisable(true);
        	txtNombreP.setText(producto.getNombre());
        	txtValor.setText(String.valueOf(producto.getValorUnitario()));
        	txtCantidad.setText(String.valueOf(producto.getCantExistenciaProducto()));
        	txtDescripcion.setText(producto.getDescripcion());
        	
        	if(producto.getFechaVencimiento() != null) {
        		checkPerecedero.setSelected(true);
        		checkPerecedero.setDisable(false);
        		dateVencimiento.setPromptText(producto.getFechaVencimiento().toString());
        		
        		lblFVencimiento.setDisable(false);
        		dateVencimiento.setDisable(false);
        		
        		checkEnvasado.setDisable(true);
        		checkEnvasado.setSelected(false);
        		checkRefrigerado.setDisable(true);
        		checkRefrigerado.setSelected(false);
        	}else {
        		checkPerecedero.setSelected(false);
    			checkPerecedero.setDisable(true);
    			
    			lblFVencimiento.setDisable(true);
        		dateVencimiento.setDisable(true);
        		dateFechaNacimiento.setPromptText("");
        	}
        		
    		if (!producto.getCodigoAprovacion().equals("N/A")) {
    			checkPerecedero.setSelected(false);
    			checkPerecedero.setDisable(false);
    			checkEnvasado.setDisable(true);
    			checkEnvasado.setSelected(false);

    			txtCodAprobacion.setText(producto.getCodigoAprovacion());
    			txtTiemRefrigeracion.setText(String.valueOf(producto.getTempRefrigeracion()));

    			checkRefrigerado.setSelected(true);
    			checkRefrigerado.setDisable(true);
    			lblCodAprovacion.setDisable(false);
    			txtCodAprobacion.setDisable(false);
    			lblTiemRefrigeracion.setDisable(false);
    			txtTiemRefrigeracion.setDisable(false);
        		
        	}else {
        		checkRefrigerado.setSelected(false);
        		checkRefrigerado.setDisable(true);
    			lblCodAprovacion.setDisable(true);
    			txtCodAprobacion.setDisable(true);
    			lblTiemRefrigeracion.setDisable(true);
    			txtTiemRefrigeracion.setDisable(true);
    			
    			txtCodAprobacion.clear();
    			txtTiemRefrigeracion.clear();
        	}
        		
    		if (producto.getPaisOrigen() != null) {
    			dateFeEnvasado.setPromptText(producto.getFechaEnvasado().toString());
    			txtPesoEnvase.setText(String.valueOf(producto.getPesoEnvase()));
    			txtPaisOrigen.getSelectionModel().select(producto.getPaisOrigen());

    			checkPerecedero.setSelected(false);
    			checkPerecedero.setDisable(false);
    			checkRefrigerado.setDisable(true);
    			checkRefrigerado.setSelected(false);

    			checkEnvasado.setSelected(true);
    			checkEnvasado.setDisable(true);
    			dateFeEnvasado.setDisable(false);
    			lblFeEnvasado.setDisable(false);
    			txtPesoEnvase.setDisable(false);
    			lblPesoEnvase.setDisable(false);
    			txtPaisOrigen.setDisable(false);
    			lblPaisOrigen.setDisable(false);
        	}else {
        		checkEnvasado.setSelected(false);
        		checkEnvasado.setDisable(true);
    			dateFeEnvasado.setDisable(true);
    			lblFeEnvasado.setDisable(true);
    			txtPesoEnvase.setDisable(true);
    			lblPesoEnvase.setDisable(true);
    			txtPaisOrigen.setDisable(true);
    			lblPaisOrigen.setDisable(true);
    			
    			txtPaisOrigen.getEditor().clear();
    			dateFeEnvasado.setPromptText("");
    			txtPesoEnvase.clear();
        	}
    		
    		actualizarProducto(producto);
    	}
    }
    
    void actualizarProducto(ProductoContenedor producto) {
    	btnActualizarProducto.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				
				producto.setCodigo(txtCodigo.getText());
				producto.setNombre(txtNombreP.getText());
				producto.setValorUnitario(Double.parseDouble(txtValor.getText()));
				producto.setCantExistenciaProducto(Integer.parseInt(txtCantidad.getText()));
				producto.setDescripcion(txtDescripcion.getText());
				
				if(checkPerecedero.isSelected()) {
					producto.setFechaVencimiento(dateVencimiento.getValue());
				}else if(checkRefrigerado.isSelected()) {
					producto.setCodigoAprovacion(txtCodAprobacion.getText());
					producto.setTempRefrigeracion(Integer.parseInt(txtTiemRefrigeracion.getText()));
				}else {
					producto.setFechaEnvasado(dateFeEnvasado.getValue());
					producto.setPesoEnvase(Integer.parseInt(txtPesoEnvase.getText()));
					producto.setPaisOrigen(txtPaisOrigen.getValue());
				}
				
				if(ModeloCrudController.getInstacia().actualizarProducto(producto)) {
					tablaProductos.refresh();
					limpiarCamposProductos(event);
				}
				
			}
		});
    }

    @FXML
    void agregarProducto(ActionEvent event) {
    	try {
    		ProductoContenedor producto = new ProductoContenedor();
    		
    		producto.setCodigo(txtCodigo.getText());
    		producto.setNombre(txtNombreP.getText());
    		producto.setDescripcion(txtDescripcion.getText());
    		producto.setValorUnitario(Double.parseDouble(txtValor.getText()));
    		producto.setCantExistenciaProducto(Integer.parseInt(txtCantidad.getText()));
    		
    		if(checkPerecedero.isSelected())producto.setFechaVencimiento(dateVencimiento.getValue());
    		else producto.setFechaVencimiento(null);
    		
    		if(checkRefrigerado.isSelected()) {
    			producto.setCodigoAprovacion(txtCodAprobacion.getText());
    			producto.setTempRefrigeracion(Integer.parseInt(txtTiemRefrigeracion.getText()));
    		}else {
    			producto.setCodigoAprovacion("N/A");
    			producto.setTempRefrigeracion(0);
    		}
    		
    		if(checkEnvasado.isSelected()) {
    			producto.setFechaEnvasado(dateFeEnvasado.getValue());
    			producto.setPesoEnvase(Integer.parseInt(txtPesoEnvase.getText()));
    			producto.setPaisOrigen(txtPaisOrigen.getValue());
    			
    		}else {
    			producto.setFechaEnvasado(null);
    			producto.setPesoEnvase(0);
    			producto.setPaisOrigen(null);
    		}
    		
    		if(producto != null) {
    			ModeloCrudController.getInstacia().agregarProducto(producto);
    			cargarDatosProducto();
    		}
    		
    		limpiarCamposProductos(event);
    		
    	}catch (ProductoDuplicadoException e) {
    		 Alert alertaDatos = new Alert(AlertType.WARNING);
             alertaDatos.setHeaderText(null);
             alertaDatos.setTitle("Datos erroneos");
             alertaDatos.setContentText(e.getMessage());
             alertaDatos.showAndWait();
    	}
    }

    @FXML
    void eliminarProducto(MouseEvent event) {
    	ProductoContenedor producto = tablaProductos.getSelectionModel().getSelectedItem();
    	
    	btnEliminarProducto.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				Alert alertaCerrar = new Alert(AlertType.CONFIRMATION);
				alertaCerrar.setHeaderText(null);
				alertaCerrar.setTitle("Eliminar");
				alertaCerrar.setContentText(MensajesInformacionConstants.PRODUCTO_CONFIRMACION);

				// limpia los botones del alert
				alertaCerrar.getButtonTypes().clear();
				// agraga botones al alert
				alertaCerrar.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

				// clase generica que captura el tipo de elemento que devuelve el panel de
				// dialogo
				Optional<ButtonType> opcion = alertaCerrar.showAndWait();
				// verifica que la opcion sea aceptar
				
				if (opcion.get() == ButtonType.OK) {
					try {
						if (ModeloCrudController.getInstacia().eliminarProducto(producto)) {
							tablaProductos.refresh();
							listaProductos.removeAll(producto);
							limpiarCamposProductos(event);

							Alert alertaDatos = new Alert(AlertType.CONFIRMATION);
							alertaDatos.setHeaderText(null);
							alertaDatos.setTitle("Informacion");
							alertaDatos.setContentText(MensajesInformacionConstants.PRODUCTO_ELIMINADO);
							alertaDatos.showAndWait();
						}
						
					} catch (CeldaNoSeleccionadaException e) {
						Alert alertaDatos = new Alert(AlertType.CONFIRMATION);
						alertaDatos.setHeaderText(null);
						alertaDatos.setTitle("Informacion");
						alertaDatos.setContentText(e.getMessage());
						alertaDatos.showAndWait();
					}
				}
			}
		});
    }
    
    @FXML
    void limpiarCamposProductos(ActionEvent event) {
    	txtCodigo.clear();
    	txtCodigo.setDisable(false);
    	txtNombreP.clear();
    	txtDescripcion.clear();
    	txtValor.clear();
    	txtCantidad.clear();
    	
    	if(checkPerecedero.isSelected()) {
    		dateVencimiento.getEditor().clear();
    		checkPerecedero.setSelected(false);
    		lblFVencimiento.setDisable(true);
    		dateVencimiento.setDisable(true);
    	}else {
    		checkPerecedero.setDisable(false);
    	}
    	
    	if(checkRefrigerado.isSelected()) {
    		txtTiemRefrigeracion.clear();
    		txtCodAprobacion.clear();
    		checkRefrigerado.setSelected(false);
    		lblCodAprovacion.setDisable(true);
    		txtCodAprobacion.setDisable(true);
    		lblTiemRefrigeracion.setDisable(true);
    		txtTiemRefrigeracion.setDisable(true);
    	}else {
    		checkRefrigerado.setDisable(false);
    	}
    	
    	if(checkEnvasado.isSelected()) {
    		dateFeEnvasado.getEditor().clear();
    		txtPesoEnvase.clear();
    		txtPaisOrigen.getEditor().clear();
    		checkEnvasado.setSelected(false);
    		dateFeEnvasado.setDisable(true);
    		lblFeEnvasado.setDisable(true);
    		txtPesoEnvase.setDisable(true);
    		lblPesoEnvase.setDisable(true);
    		txtPaisOrigen.setDisable(true);
    		lblPaisOrigen.setDisable(true);
    	}else {
    		checkEnvasado.setDisable(false);
    	}
    }
    
    public void cargarDatosProducto() {
    	productoContenedor = new ProductoContenedor();
    	
    	for (ProductoContenedor producto : listaProductosArray) {
    		
    		mostrarCamposProductos();
    		
    		productoContenedor.setCodigo(producto.getCodigo());
    		productoContenedor.setNombre(producto.getNombre());
    		productoContenedor.setDescripcion(producto.getDescripcion());
    		productoContenedor.setValorUnitario(producto.getValorUnitario());
    		productoContenedor.setCantExistenciaProducto(producto.getCantExistenciaProducto());
    		
    		if(checkPerecedero.isSelected()) productoContenedor.setFechaVencimiento(producto.getFechaVencimiento());
    		else productoContenedor.setFechaVencimiento(null);
    		
    		if(checkRefrigerado.isSelected()) {
    			productoContenedor.setCodigoAprovacion(producto.getCodigoAprovacion());
    			productoContenedor.setTempRefrigeracion(producto.getTempRefrigeracion());
    		}else {
    			productoContenedor.setCodigoAprovacion("N/A");
    			productoContenedor.setTempRefrigeracion(0);
    		}
    		
    		if(checkEnvasado.isSelected()) {
    			productoContenedor.setFechaEnvasado(producto.getFechaEnvasado());
    			productoContenedor.setPesoEnvase(producto.getPesoEnvase());
    			productoContenedor.setPaisOrigen(producto.getPaisOrigen());
    		}else {
    			productoContenedor.setFechaEnvasado(null);
    			productoContenedor.setPesoEnvase(0);
    			productoContenedor.setPaisOrigen(null);
    		}
		}
    	
    	listaProductos.add(productoContenedor);
    	tablaProductos.setItems(listaProductos);
    }
    
    public void mostrarCamposProductos() {
    	colCodigo.setCellValueFactory(
                new PropertyValueFactory<>("codigo"));
    	colNombreP.setCellValueFactory(
                new PropertyValueFactory<>("nombre"));
        colDescripcion.setCellValueFactory(
                new PropertyValueFactory<>("descripcion"));
        colValor.setCellValueFactory(
                new PropertyValueFactory<>("valorUnitario"));
        colCantidad.setCellValueFactory(
                new PropertyValueFactory<>("cantExistenciaProducto"));
        colFeVencimiento.setCellValueFactory(
                new PropertyValueFactory<>("fechaVencimiento"));
        colCodAprovacion.setCellValueFactory(
                new PropertyValueFactory<>("codigoAprovacion"));
        colTiemRefrigeracion.setCellValueFactory(
                new PropertyValueFactory<>("tempRefrigeracion"));
        colFeEnvasado.setCellValueFactory(new 
        		PropertyValueFactory<>("fechaEnvasado"));
        colPesoEnvase.setCellValueFactory(new 
        		PropertyValueFactory<>("pesoEnvase"));
        colPaisOrigen.setCellValueFactory(new 
        		PropertyValueFactory<>("paisOrigen"));
    }
    
    public ObservableList<PaisOrigenEnum> getPaises(){
    	for (PaisOrigenEnum pais : PaisOrigenEnum.values()) {
			listaPaises.add(pais);
		}
    	
    	return listaPaises;
    }
    
    @FXML
    public void habilitarProductoPerecedero(MouseEvent e) {
    	if(checkPerecedero.isSelected()) {
    		lblFVencimiento.setDisable(false);
    		dateVencimiento.setDisable(false);
    		
    		lblCodAprovacion.setDisable(true);
    		txtCodAprobacion.setDisable(true);
    		lblTiemRefrigeracion.setDisable(true);
    		txtTiemRefrigeracion.setDisable(true);
    		dateFeEnvasado.setDisable(true);
    		lblFeEnvasado.setDisable(true);
    		txtPesoEnvase.setDisable(true);
    		lblPesoEnvase.setDisable(true);
    		txtPaisOrigen.setDisable(true);
    		lblPaisOrigen.setDisable(true);
    		

			checkRefrigerado.setDisable(true);
			checkEnvasado.setDisable(true);
    	}else {
    		lblFVencimiento.setDisable(true);
    		dateVencimiento.setDisable(true);
    		

			checkRefrigerado.setDisable(false);
			checkEnvasado.setDisable(false);
    	}
    }
    
    @FXML
    public void habilitarProductoRefrigerado(MouseEvent e) {
    	if(checkRefrigerado.isSelected()) {
    		lblCodAprovacion.setDisable(false);
    		txtCodAprobacion.setDisable(false);
    		lblTiemRefrigeracion.setDisable(false);
    		txtTiemRefrigeracion.setDisable(false);
    		
    		dateFeEnvasado.setDisable(true);
    		lblFeEnvasado.setDisable(true);
    		txtPesoEnvase.setDisable(true);
    		lblPesoEnvase.setDisable(true);
    		txtPaisOrigen.setDisable(true);
    		lblPaisOrigen.setDisable(true);
    		lblFVencimiento.setDisable(true);
    		dateVencimiento.setDisable(true);
    		
			checkPerecedero.setDisable(true);
			checkEnvasado.setDisable(true);
    	}else {
    		lblCodAprovacion.setDisable(true);
    		txtCodAprobacion.setDisable(true);
    		lblTiemRefrigeracion.setDisable(true);
    		txtTiemRefrigeracion.setDisable(true);

			checkPerecedero.setDisable(false);
			checkEnvasado.setDisable(false);
    	}
    }
    
    @FXML
    public void habilitarProductoEnvasado(MouseEvent e) {
    	if(checkEnvasado.isSelected()) {
    		dateFeEnvasado.setDisable(false);
    		lblFeEnvasado.setDisable(false);
    		txtPesoEnvase.setDisable(false);
    		lblPesoEnvase.setDisable(false);
    		txtPaisOrigen.setDisable(false);
    		lblPaisOrigen.setDisable(false);
    		
    		lblCodAprovacion.setDisable(true);
    		txtCodAprobacion.setDisable(true);
    		lblTiemRefrigeracion.setDisable(true);
    		txtTiemRefrigeracion.setDisable(true);
    		lblFVencimiento.setDisable(true);
    		dateVencimiento.setDisable(true);
    		
    		checkPerecedero.setDisable(true);
			checkRefrigerado.setDisable(true);
    	}else {
    		dateFeEnvasado.setDisable(true);
    		lblFeEnvasado.setDisable(true);
    		txtPesoEnvase.setDisable(true);
    		lblPesoEnvase.setDisable(true);
    		txtPaisOrigen.setDisable(true);
    		lblPaisOrigen.setDisable(true);
    		
    		checkPerecedero.setDisable(false);
			checkRefrigerado.setDisable(false);
    	}
    }
    
    /**
     * ###############################################################################
     * ###############################ACCIONES VENTAS######################################
     * ##############################################################################
     */
    
    
    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		clientesJuridicos =
    			ModeloCrudController.getInstacia().getListaClientesJuridicos();
		clientesNaturales =
    			ModeloCrudController.getInstacia().getListaClientesNaturales();
		listaProductosArray =
				ModeloCrudController.getInstacia().getListProductos();
		
		listaClientes = FXCollections.observableArrayList();
		listaProductos = FXCollections.observableArrayList();
		listaPaises = FXCollections.observableArrayList();
		
		txtPaisOrigen.setItems(getPaises());
	}

}
