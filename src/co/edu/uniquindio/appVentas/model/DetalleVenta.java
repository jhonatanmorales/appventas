/**
 * 
 */
package co.edu.uniquindio.appVentas.model;

/**
 * @author jhomora
 *
 */
public class DetalleVenta {

	private int cantProductosVendidos;
	private double subTotal;
	private Producto productoVendido;
	
	/**
	 * @param cantProductosVendidos
	 * @param subTotal
	 * @param productoVendido
	 */
	public DetalleVenta(int cantProductosVendidos, double subTotal, Producto productoVendido) {
		this.cantProductosVendidos = cantProductosVendidos;
		this.subTotal = subTotal;
		this.productoVendido = productoVendido;
	}

	/**
	 * @return the cantProductosVendidos
	 */
	public int getCantProductosVendidos() {
		return cantProductosVendidos;
	}

	/**
	 * @param cantProductosVendidos the cantProductosVendidos to set
	 */
	public void setCantProductosVendidos(int cantProductosVendidos) {
		this.cantProductosVendidos = cantProductosVendidos;
	}

	/**
	 * @return the subTotal
	 */
	public double getSubTotal() {
		return subTotal;
	}

	/**
	 * @param subTotal the subTotal to set
	 */
	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}

	/**
	 * @return the productoVendido
	 */
	public Producto getProductoVendido() {
		return productoVendido;
	}

	/**
	 * @param productoVendido the productoVendido to set
	 */
	public void setProductoVendido(Producto productoVendido) {
		this.productoVendido = productoVendido;
	}

	@Override
	public String toString() {
		return "DetalleVenta [cantProductosVendidos=" + cantProductosVendidos + ", subTotal=" + subTotal
				+ ", productoVendido=" + productoVendido + "]";
	}
	
	
}
