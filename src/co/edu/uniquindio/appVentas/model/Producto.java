/**
 * 
 */
package co.edu.uniquindio.appVentas.model;

/**
 * @author jhomora
 *
 */
public abstract class Producto {

	private String codigo;
	private String nombre;
	private String descripcion;
	private double valorUnitario;
	private int cantExistenciaProducto;
	
	/**
	 * 
	 */
	public Producto() {
		
	}
	
	/**
	 * @param codigo
	 * @param nombre
	 * @param descripcion
	 * @param valorUnitario
	 * @param cantExistenciaProducto
	 */
	public Producto(String codigo, String nombre, String descripcion, double valorUnitario,
			int cantExistenciaProducto) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.valorUnitario = valorUnitario;
		this.cantExistenciaProducto = cantExistenciaProducto;
	}


	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}


	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}


	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}


	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	/**
	 * @return the valorUnitario
	 */
	public double getValorUnitario() {
		return valorUnitario;
	}


	/**
	 * @param valorUnitario the valorUnitario to set
	 */
	public void setValorUnitario(double valorUnitario) {
		this.valorUnitario = valorUnitario;
	}


	/**
	 * @return the cantExistenciaProducto
	 */
	public int getCantExistenciaProducto() {
		return cantExistenciaProducto;
	}


	/**
	 * @param cantExistenciaProducto the cantExistenciaProducto to set
	 */
	public void setCantExistenciaProducto(int cantExistenciaProducto) {
		this.cantExistenciaProducto = cantExistenciaProducto;
	}

}
