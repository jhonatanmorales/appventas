/**
 * 
 */
package co.edu.uniquindio.appVentas.controller;

import java.util.ArrayList;

import co.edu.uniquindio.appVentas.constants.MensajesExcepcionesConstants;
import co.edu.uniquindio.appVentas.constants.MensajesInformacionConstants;
import co.edu.uniquindio.appVentas.exception.CeldaNoSeleccionadaException;
import co.edu.uniquindio.appVentas.exception.ClienteDuplicadoException;
import co.edu.uniquindio.appVentas.exception.ProductoDuplicadoException;
import co.edu.uniquindio.appVentas.model.ClienteContenedor;
import co.edu.uniquindio.appVentas.model.PersonaJuridica;
import co.edu.uniquindio.appVentas.model.PersonaNatural;
import co.edu.uniquindio.appVentas.model.ProductoContenedor;
import co.edu.uniquindio.appVentas.model.ProductoEnvasado;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * @author jhomora
 *
 */
public class ModeloCrudController {

	private static ModeloCrudController instacia;
	public ArrayList<PersonaNatural> listaClientesNaturales;
	public ArrayList<PersonaJuridica> listaClientesJuridicos;
	public ArrayList<ProductoContenedor> listaProductos;

	/**
	 * 
	 */
	private ModeloCrudController() {
		listaClientesNaturales = new ArrayList<PersonaNatural>();
		listaClientesJuridicos = new ArrayList<PersonaJuridica>();
		listaProductos = new ArrayList<ProductoContenedor>();
	}

	/**
	 * 
	 * @return
	 */
	public static ModeloCrudController getInstacia() {
		if (instacia == null) {
			instacia = new ModeloCrudController();
			return instacia;
		}

		return instacia;
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<PersonaNatural> getListaClientesNaturales() {
		return listaClientesNaturales;
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<PersonaJuridica> getListaClientesJuridicos() {
		return listaClientesJuridicos;
	}
	

	/**
	 * 
	 * @param clienteNatural
	 * @throws ClienteDuplicadoException
	 */
	public void agregarClienteNatural(PersonaNatural clienteNatural) throws ClienteDuplicadoException {

		for (PersonaNatural personaNatural : listaClientesNaturales) {
			if (personaNatural.getDocumento().equals(clienteNatural.getDocumento())) {
				throw new ClienteDuplicadoException(MensajesExcepcionesConstants.CLIENTE_DUPLICADO);
			}
		}

		listaClientesNaturales.add(clienteNatural);

		Alert alertaDatos = new Alert(AlertType.INFORMATION);
		alertaDatos.setHeaderText(null);
		alertaDatos.setTitle("Informacion");
		alertaDatos.setContentText(MensajesInformacionConstants.CLIENTE_AGREGADO);
		alertaDatos.showAndWait();
	}

	/**
	 * 
	 * @param clienteJuridico
	 * @throws ClienteDuplicadoException
	 */
	public void agregarClienteJuridico(PersonaJuridica clienteJuridico) throws ClienteDuplicadoException {

		for (PersonaJuridica personaJuridica : listaClientesJuridicos) {
			if (personaJuridica.getNit().equals(clienteJuridico.getNit())) {
				throw new ClienteDuplicadoException(MensajesExcepcionesConstants.CLIENTE_DUPLICADO);
			}
		}

		listaClientesJuridicos.add(clienteJuridico);

		Alert alertaDatos = new Alert(AlertType.INFORMATION);
		alertaDatos.setHeaderText(null);
		alertaDatos.setTitle("Informacion");
		alertaDatos.setContentText(MensajesInformacionConstants.CLIENTE_AGREGADO);
		alertaDatos.showAndWait();
	}

	/**
	 * 
	 * @param clienteNatural
	 */
	public boolean eliminarClienteNatural(String documento) {
		boolean isEliminado = false;

		for (int i = 0; i < listaClientesNaturales.size(); i++) {
			if (listaClientesNaturales.get(i).getDocumento().equals(documento)) {
				listaClientesNaturales.remove(i);
				isEliminado = true;
			}
		}

		return isEliminado;
	}

	/**
	 * 
	 * @param clienteJuridico
	 */
	public boolean eliminarClienteJuridico(String nit) {
		boolean isEliminado = false;

		for (int i = 0; i < listaClientesJuridicos.size(); i++) {
			if (listaClientesJuridicos.get(i).getNit().equals(nit)) {
				listaClientesJuridicos.remove(i);
				isEliminado = true;
			}
		}

		return isEliminado;
	}

	public boolean actualizarCliente(ClienteContenedor clienteContenedor) {
		boolean isActualizado = false;

		for (int i = 0; i < listaClientesNaturales.size(); i++) {
			if (listaClientesNaturales.get(i).getDocumento().equals(clienteContenedor.getDocumento()) &&
					clienteContenedor.getNit().equals("N/A")) {
				listaClientesNaturales.get(i).setNombres(clienteContenedor.getNombres());
				listaClientesNaturales.get(i).setApellidos(clienteContenedor.getApellidos());
				listaClientesNaturales.get(i).setTelefono(clienteContenedor.getTelefono());
				listaClientesNaturales.get(i).setDireccion(clienteContenedor.getDireccion());
				listaClientesNaturales.get(i).setEmail(clienteContenedor.getEmail());
				listaClientesNaturales.get(i).setFechaNacimiento(clienteContenedor.getFechaNacimiento());
				
				isActualizado = true;
			}
		}
		
		for (int i = 0; i < listaClientesJuridicos.size(); i++) {
			if (listaClientesJuridicos.get(i).getNit().equals(clienteContenedor.getNit()) &&
					!clienteContenedor.getNit().equals("N/A")) {
				listaClientesJuridicos.get(i).setNombres(clienteContenedor.getNombres());
				listaClientesJuridicos.get(i).setApellidos(clienteContenedor.getApellidos());
				listaClientesJuridicos.get(i).setTelefono(clienteContenedor.getTelefono());
				listaClientesJuridicos.get(i).setDireccion(clienteContenedor.getDireccion());
				listaClientesJuridicos.get(i).setNit(clienteContenedor.getNit());
				
				isActualizado = true;
				
			}
		}
		
		if(isActualizado) {
			Alert alertaDatos = new Alert(AlertType.INFORMATION);
			alertaDatos.setHeaderText(null);
			alertaDatos.setTitle("Informacion");
			alertaDatos.setContentText(MensajesInformacionConstants.CLIENTE_ACTUALIZAR);
			alertaDatos.showAndWait();
		}

		return isActualizado;
	}
	
	/**
     * ###############################################################################
     * ###############################ACCIONES PRODUCTOS######################################
     * ##############################################################################
     */
	
	/**
	 * 
	 * @return
	 */
	public ArrayList<ProductoContenedor> getListProductos(){
		return listaProductos;
	}
	
	public void agregarProducto(ProductoContenedor productoContenedor) throws ProductoDuplicadoException {

		for (ProductoContenedor producto : listaProductos) {
			if (producto.getCodigo().equals(productoContenedor.getCodigo())) {
				throw new ProductoDuplicadoException(MensajesExcepcionesConstants.PRODUCTO_DUPLICADO);
			}
		}

		listaProductos.add(productoContenedor);

		Alert alertaDatos = new Alert(AlertType.INFORMATION);
		alertaDatos.setHeaderText(null);
		alertaDatos.setTitle("Informacion");
		alertaDatos.setContentText(MensajesInformacionConstants.PRODUCTO_AGREGADO);
		alertaDatos.showAndWait();
	}
	
	/**
	 * 
	 * @param productoContenedor
	 * @return
	 */
	public boolean eliminarProducto(ProductoContenedor productoContenedor) throws CeldaNoSeleccionadaException{
		boolean isEliminado = false;

		if(productoContenedor != null) {
			for (int i = 0; i < listaProductos.size(); i++) {
				if (listaProductos.get(i).getCodigo().equals(productoContenedor.getCodigo())) {
					listaProductos.remove(i);
					isEliminado = true;
				}
			}
		}else {
			throw new CeldaNoSeleccionadaException(MensajesExcepcionesConstants.NO_SELECCION_PRODUCTO);
		}

		return isEliminado;
	}
	
	public boolean actualizarProducto(ProductoContenedor productoContenedor) {
		boolean isActualizado = false;

		for (int i = 0; i < listaProductos.size(); i++) {
			if (listaProductos.get(i).getCodigo().equals(productoContenedor.getCodigo())) {
				
				
				listaProductos.get(i).setCodigo(productoContenedor.getCodigo());
				listaProductos.get(i).setNombre(productoContenedor.getNombre());
				listaProductos.get(i).setDescripcion(productoContenedor.getDescripcion());
				listaProductos.get(i).setValorUnitario(productoContenedor.getValorUnitario());
				listaProductos.get(i).setCantExistenciaProducto(productoContenedor.getCantExistenciaProducto());
				
				listaProductos.get(i).setFechaVencimiento(productoContenedor.getFechaVencimiento());
				
				listaProductos.get(i).setCodigoAprovacion(productoContenedor.getCodigoAprovacion());
				listaProductos.get(i).setTempRefrigeracion(productoContenedor.getTempRefrigeracion());
				
				listaProductos.get(i).setFechaEnvasado(productoContenedor.getFechaEnvasado());
				listaProductos.get(i).setPesoEnvase(productoContenedor.getPesoEnvase());
				listaProductos.get(i).setPaisOrigen(productoContenedor.getPaisOrigen());
				
				isActualizado = true;
			}
		}
		
		
		if(isActualizado) {
			Alert alertaDatos = new Alert(AlertType.INFORMATION);
			alertaDatos.setHeaderText(null);
			alertaDatos.setTitle("Informacion");
			alertaDatos.setContentText(MensajesInformacionConstants.PRODUCTO_ACTUALIZAR);
			alertaDatos.showAndWait();
		}

		return isActualizado;
	}

}
