module appVentas {
	requires javafx.controls;
	requires javafx.fxml;
	exports co.edu.uniquindio.appVentas.controller;
	
	opens co.edu.uniquindio.appVentas to javafx.graphics, javafx.fxml;
	opens co.edu.uniquindio.appVentas.controller to javafx.graphics, javafx.fxml;
	opens co.edu.uniquindio.appVentas.model to javafx.graphics, javafx.fxml, javafx.base;
}
