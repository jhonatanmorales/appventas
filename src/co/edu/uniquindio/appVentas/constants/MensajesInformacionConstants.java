/**
 * 
 */
package co.edu.uniquindio.appVentas.constants;

/**
 * @author jhomora
 *
 */
public class MensajesInformacionConstants {

	public static final String CLIENTE_AGREGADO = "Se agregó el cliente correctamente";
	public static final String CLIENTE_ELIMINADO = "Se eliminó el cliente correctamente";
	public static final String CLIENTE_CONFIRMACION = "¿Seguro desea eliminar el cliente?";
	public static final String CLIENTE_ACTUALIZAR= "Cliente actualizado con exito";

	
	/**
	 * PRODUCTO
	 */
	public static final String PRODUCTO_AGREGADO = "Se agregó el producto correctamente";
	public static final String PRODUCTO_ELIMINADO = "Se eliminó el producto correctamente";
	public static final String PRODUCTO_CONFIRMACION = "¿Seguro desea eliminar el producto?";
	public static final String PRODUCTO_ACTUALIZAR= "producto actualizado con exito";
}
