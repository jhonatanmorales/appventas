/**
 * 
 */
package co.edu.uniquindio.appVentas.model;

/**
 * @author jhomora
 *
 */
public class ProductoRefrigerado extends Producto {

	private String codigoAprovacion;
	private int tempRefrigeracion;
	
	/**
	 * @param codigo
	 * @param nombre
	 * @param descripcion
	 * @param valorUnitario
	 * @param cantExistenciaProducto
	 */
	public ProductoRefrigerado(String codigo, String nombre, String descripcion, int valorUnitario,
			int cantExistenciaProducto, String codigoAprovacion, int tempRefrigeracion) {
		super(codigo, nombre, descripcion, valorUnitario, cantExistenciaProducto);

		this.codigoAprovacion = codigoAprovacion;
		this.tempRefrigeracion = tempRefrigeracion;
	}

	/**
	 * @return the codigoAprovacion
	 */
	public String getCodigoAprovacion() {
		return codigoAprovacion;
	}

	/**
	 * @param codigoAprovacion the codigoAprovacion to set
	 */
	public void setCodigoAprovacion(String codigoAprovacion) {
		this.codigoAprovacion = codigoAprovacion;
	}

	/**
	 * @return the temperaturaRefrigeracion
	 */
	public int getTemperaturaRefrigeracion() {
		return tempRefrigeracion;
	}

	/**
	 * @param temperaturaRefrigeracion the temperaturaRefrigeracion to set
	 */
	public void setTemperaturaRefrigeracion(int temperaturaRefrigeracion) {
		this.tempRefrigeracion = temperaturaRefrigeracion;
	}

	
	
}
